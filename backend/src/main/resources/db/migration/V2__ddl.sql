INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (1, 'Ваше обращение связано с ...', null, null, null, false, null);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (2, 'Укажите категорию',  'Защита прав потребителей', 1, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (3,  null, 'Образование', 1, null, false, 2);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (4,  null, 'Здравоохранение', 1, null, false, 3);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (5,  null,'ЖКХ', 1, null, false, 4);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (6,   null, 'Транспорт', 1, null, false, 5);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (7,  null, 'Деятельность гос.органов', 1, null, false, 6);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (8, 'С чем связано обращение',  'товар', 2, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (9, null, 'Работа', 2, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (10, null, 'Услуга', 2, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (11, null, 'Качество товара', 8, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (12, null, 'Сроки оказания услуг (выполнения работ)', 8, null, false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (13, null, 'Замена товара', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (14, null, 'Замена на альтернативный товар с перерасчетом', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (15, null, 'Уменьшение цены', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (16, null, 'Устранение недостатков', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (17, null, 'Возмещение расходов', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);
INSERT INTO nodes (id, question, bundle_text, parent_id, additional_info, is_custom, target_service_id) VALUES (18, null, 'Возврат товара и денежных средств', 11, '{ "date": { "label": "Дата покупки", "type": "date" }, "merchantName": { "label": "Продавец", "type": "text" }, "additionalInfo": { "label": "Дополнительная информация", "type": "text" } }', false, 1);

ALTER sequence nodes_id_seq restart WITH 19;

INSERT INTO users (id, birth_date, birth_place, citizenship, email, email_verified, identity_document, image_url, is_male, name, password, phone, provider, provider_id, snils) VALUES (1, null, null, null, 'slebedkin90@gmail.com', false, null, null, false, 'Станислав Лебедкин', '$2a$10$GD4h9GcHbcCmFYsWuVbpqeRGzaDgyC51golklfGSUid9WUh0ttI06', null, 'local', null, null);
INSERT INTO users (id, birth_date, birth_place, citizenship, email, email_verified, identity_document, image_url, is_male, name, password, phone, provider, provider_id, snils) VALUES (2, '2016-07-27 19:39:25.000000', 'RUSSIA', 'RUSSIA', 'test@gmail.com', false, '1111111111', null, false, 'Тест Тестович', '$2a$10$GD4h9GcHbcCmFYsWuVbpqeRGzaDgyC51golklfGSUid9WUh0ttI06', null, 'local', null, '123');

ALTER sequence users_id_seq restart WITH 3;

INSERT INTO services(id, name) VALUES (1, 'Роспотребнадзор');
INSERT INTO services(id, name) VALUES (2, 'Департамент образования');
INSERT INTO services(id, name) VALUES (3, 'Департамент здравоохранения');
INSERT INTO services(id, name) VALUES (4, 'Объединенная диспетчерская служба');
INSERT INTO services(id, name) VALUES (5, 'Департамент транспорта');
INSERT INTO services(id, name) VALUES (6, 'Управление внутренних дел');
INSERT INTO services(id, name) VALUES (7, 'Оператор ручной обработки обращений');

ALTER sequence services_id_seq restart WITH 8;