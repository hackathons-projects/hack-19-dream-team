-- auto-generated definition
create table nodes
(
    id              bigserial not null
        constraint nodes_pkey
            primary key,
    additional_info varchar(255),
    bundle_text     varchar(255),
    is_custom       boolean,
    question        varchar(255),
    weight          bigint,
    parent_id       bigint
        constraint fkml83dqkjygb0d97yn62yn1so8
            references nodes,
    target_service_id       bigint
        constraint fkml83dqkjygb0d97yn62yn1so9
            references nodes

);

alter table nodes
    owner to postgres;



-- auto-generated definition
create table users
(
    id                bigserial    not null
        constraint users_pkey
            primary key,
    birth_date        timestamp,
    birth_place       varchar(255),
    citizenship       varchar(255),
    email             varchar(255) not null
        constraint uk6dotkott2kjsp8vw4d0m25fb7
            unique,
    email_verified    boolean      not null,
    identity_document varchar(255),
    image_url         varchar(255),
    is_male           boolean,
    name              varchar(255) not null,
    password          varchar(255),
    phone             varchar(255),
    provider          varchar(255) not null,
    provider_id       varchar(255),
    snils             varchar(255)
);

alter table users
    owner to postgres;

create table services(
        id                bigserial    not null
        constraint services_pkey
            primary key,
        name       varchar(255)
);

alter table services
    owner to postgres;

create table requests
(
    id              bigserial not null
        constraint requests_pkey
            primary key,
    creator_id       bigint
        constraint fkml83dqkjygb0d97yn62yn1soa
            references users,
    target_service_id       bigint
        constraint fkml83dqkjygb0d97yn62yr1soa
            references services,
    content varchar(3000),
    created_date      bigint    not null,
    modified_date     bigint,
    status     varchar(25)
);

alter table requests
    owner to postgres;

-- auto-generated definition
create table revinfo
(
    rev      integer not null
        constraint revinfo_pkey
            primary key,
    revtstmp bigint
);

alter table revinfo
    owner to postgres;


-- auto-generated definition
create table requests_aud
(
    id                bigint  not null,
    rev               integer not null
        constraint fkjdms2p6o3o9oblqshtn3xahy
            references revinfo,
    revtype           smallint,
    content           varchar(255),
    created_date      bigint,
    modified_date     bigint,
    status            varchar(255),
    target_service_id bigint,
    constraint requests_aud_pkey
        primary key (id, rev)
);

alter table requests_aud
    owner to postgres;

-- auto-generated definition
create table services_aud
(
    id      bigint  not null,
    rev     integer not null
        constraint fkde4u04qyn9331jwed2f1o1r87
            references revinfo,
    revtype smallint,
    name    varchar(255),
    constraint services_aud_pkey
        primary key (id, rev)
);

alter table services_aud
    owner to postgres;

-- auto-generated definition
create sequence hibernate_sequence;
alter sequence hibernate_sequence owner to postgres;