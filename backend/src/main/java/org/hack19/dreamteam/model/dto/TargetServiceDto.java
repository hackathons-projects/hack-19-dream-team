package org.hack19.dreamteam.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hack19.dreamteam.db.model.TargetServiceEntity;

@Getter
@Setter
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
@NoArgsConstructor
public class TargetServiceDto {
    private Long id;
    private String name;

    public static TargetServiceDto of(TargetServiceEntity targetServiceEntity) {
        return new TargetServiceDto(
                targetServiceEntity.getId(),
                targetServiceEntity.getName()
        );
    }
}
