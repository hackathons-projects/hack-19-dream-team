package org.hack19.dreamteam.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hack19.dreamteam.db.model.NodeEntity;
import org.hack19.dreamteam.util.NodeVisibilityUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
@NoArgsConstructor
public class NodeDto {

    private Long id;

    private String question;

    private String bundleText;

    private List<NodeDto> children;

    private String additionalInfo;

    private Long parentId;

    private Long weight;

    private Boolean isCustom;

    private Boolean isVisible;

    private Long targetServiceId;

    private String targetServiceName;

    public static NodeDto of(NodeEntity nodeEntity) {
        NodeEntity parent = nodeEntity.getParent();
        return new NodeDto(
                nodeEntity.getId(),
                nodeEntity.getQuestion(),
                nodeEntity.getBundleText(),
                new ArrayList<>(),
                nodeEntity.getAdditionalInfo(),
                parent == null ? null : parent.getId(),
                Optional.ofNullable(nodeEntity.getWeight()).orElse(0L),
                Optional.ofNullable(nodeEntity.getIsCustom()).orElse(false),
                NodeVisibilityUtils.isNodeVisible(nodeEntity),
                nodeEntity.getTargetService() == null ? null : nodeEntity.getTargetService().getId(),
                nodeEntity.getTargetService() == null ? null : nodeEntity.getTargetService().getName()
        );

    }
}
