package org.hack19.dreamteam.model.payload;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
@NoArgsConstructor
public class CreateRequestRequest {

    private Long targetServiceId;

    private String content;


}
