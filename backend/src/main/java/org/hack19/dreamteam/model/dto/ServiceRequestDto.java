package org.hack19.dreamteam.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hack19.dreamteam.db.model.RequestStatus;
import org.hack19.dreamteam.db.model.ServiceRequestEntity;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
@NoArgsConstructor
public class ServiceRequestDto {

    private Long id;

    private String email;

    private Long targetServiceId;

    private String content;

    private RequestStatus status;

    private Date creationDate;

    private Date modifiedDate;

    public static ServiceRequestDto of(ServiceRequestEntity result) {
        return new ServiceRequestDto(
                result.getId(),
                result.getCreator().getEmail(),
                result.getTargetService().getId(),
                result.getContent(),
                result.getStatus(),
                new Date(result.getCreatedDate()),
                new Date(result.getModifiedDate())
        );
    }
}
