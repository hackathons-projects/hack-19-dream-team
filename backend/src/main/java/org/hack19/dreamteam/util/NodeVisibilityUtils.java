package org.hack19.dreamteam.util;

import org.hack19.dreamteam.db.model.NodeEntity;

import java.util.Optional;

public class NodeVisibilityUtils {

    private static final long VISIBILITY_THRESHOLD = 3L;

    public static boolean isNodeVisible(NodeEntity nodeEntity) {
        return !nodeEntity.getIsCustom() ||
                Optional.ofNullable(nodeEntity.getWeight()).orElse(0L) > VISIBILITY_THRESHOLD;
    }
}
