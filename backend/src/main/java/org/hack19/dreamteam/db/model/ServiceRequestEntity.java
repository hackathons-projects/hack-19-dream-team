package org.hack19.dreamteam.db.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
@Table(name = "requests")
public class ServiceRequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn
    @NotAudited
    private UserEntity creator;

    @ManyToOne
    @JoinColumn
//    @NotAudited
    private TargetServiceEntity targetService;

    @Column
    private String content;

    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;
}
