package org.hack19.dreamteam.db.model;

public enum OAuthProvider {
    local,
    facebook,
    google,
    github
}
