package org.hack19.dreamteam.db.repository;

import org.hack19.dreamteam.db.model.NodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NodeRepository extends JpaRepository<NodeEntity, Long> {

    NodeEntity findByParentNull();

    List<NodeEntity> findAllByParent(NodeEntity parent);
}
