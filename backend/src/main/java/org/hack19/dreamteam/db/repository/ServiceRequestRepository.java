package org.hack19.dreamteam.db.repository;

import org.hack19.dreamteam.db.model.ServiceRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRequestRepository extends JpaRepository<ServiceRequestEntity, Long> {
}
