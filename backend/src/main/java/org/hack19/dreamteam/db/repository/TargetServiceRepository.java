package org.hack19.dreamteam.db.repository;

import org.hack19.dreamteam.db.model.TargetServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetServiceRepository extends JpaRepository<TargetServiceEntity, Long> {
}
