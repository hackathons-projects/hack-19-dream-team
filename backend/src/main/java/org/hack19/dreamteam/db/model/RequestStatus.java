package org.hack19.dreamteam.db.model;

public enum RequestStatus {
    CREATED, PENDING, IN_PROGRESS, SUCCESS
}
