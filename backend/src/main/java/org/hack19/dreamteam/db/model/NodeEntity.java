package org.hack19.dreamteam.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "nodes")
public class NodeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String question;

    @Column
    private String bundleText;

    @ManyToOne
    @JoinColumn
    private NodeEntity parent;

    @Column
    private String additionalInfo;

    @Column
    private Long weight;

    @Column
    private Boolean isCustom = false;

    @ManyToOne
    @JoinColumn
    private TargetServiceEntity targetService;
}
