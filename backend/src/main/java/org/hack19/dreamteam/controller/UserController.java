package org.hack19.dreamteam.controller;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.exception.ResourceNotFoundException;
import org.hack19.dreamteam.db.model.UserEntity;
import org.hack19.dreamteam.db.repository.UserRepository;
import org.hack19.dreamteam.config.security.CurrentUser;
import org.hack19.dreamteam.config.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class UserController {

    private final UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserEntity getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }
}
