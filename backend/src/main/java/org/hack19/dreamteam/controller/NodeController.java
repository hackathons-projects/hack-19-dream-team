package org.hack19.dreamteam.controller;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.model.dto.NodeDto;
import org.hack19.dreamteam.service.NodeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/nodes")
public class NodeController {

    private final NodeService nodeService;

    @GetMapping
    public ResponseEntity<NodeDto> getNodeTree() {
        return ResponseEntity.ok(nodeService.getStructureTree());
    }

    @PostMapping
    public ResponseEntity<NodeDto> createCustomNode(@RequestBody NodeDto nodeDto) {
        return ResponseEntity.ok(nodeService.createCustomNode(nodeDto));
    }

    @PutMapping
    public ResponseEntity<NodeDto> updateCustomNode(@RequestBody NodeDto nodeDto) {
        return ResponseEntity.ok(nodeService.updateNode(nodeDto));
    }

    @PostMapping("/increment/{nodeId}")
    public ResponseEntity<Long> increment(@PathVariable("nodeId") Long nodeId) {
        return ResponseEntity.ok(nodeService.incrementWeight(nodeId));
    }
}
