package org.hack19.dreamteam.controller;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.config.security.UserPrincipal;
import org.hack19.dreamteam.model.dto.ServiceRequestDto;
import org.hack19.dreamteam.model.payload.CreateRequestRequest;
import org.hack19.dreamteam.service.ServiceRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/requests")
public class ServiceRequestController {

    private final ServiceRequestService serviceRequestService;

    @GetMapping
    public ResponseEntity<List<ServiceRequestDto>> getAllRequests() {
        return ResponseEntity.ok(serviceRequestService.getAllRequests());
    }

    @PostMapping
    public ResponseEntity<ServiceRequestDto> createRequest(@RequestBody CreateRequestRequest createRequestRequest, Principal principal) {
        String email = ((UserPrincipal)((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getEmail();
        ServiceRequestDto request = serviceRequestService.createRequest(createRequestRequest, email);
        return ResponseEntity.ok(serviceRequestService.processRequest(request));
    }

//    @PutMapping
//    public ResponseEntity<ServiceRequestDto> updateRequest(@RequestBody ServiceRequestDto serviceRequestDto) {
//        return ResponseEntity.ok(serviceRequestService.updateRequest(serviceRequestDto));
//    }
//
//    @DeleteMapping
//    public ResponseEntity<ServiceRequestDto> cancelRequest(@RequestBody ServiceRequestDto serviceRequestDto) {
//        return ResponseEntity.ok(serviceRequestService.cancelRequest(serviceRequestDto));
//    }

    @GetMapping("/{requestId}")
    public ResponseEntity<List<ServiceRequestDto>> history(@PathVariable(value = "requestId") Long requestId) {
        return ResponseEntity.ok(serviceRequestService.history(requestId));
    }

}
