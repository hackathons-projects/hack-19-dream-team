package org.hack19.dreamteam.controller;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.model.dto.TargetServiceDto;
import org.hack19.dreamteam.service.TargetServiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/targetServices")
public class TargetServiceController {

    private final TargetServiceService targetServiceService;

    @GetMapping
    public ResponseEntity<List<TargetServiceDto>> getNodeTree() {
        return ResponseEntity.ok(targetServiceService.getAllTargetServices());
    }

}
