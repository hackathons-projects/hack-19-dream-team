package org.hack19.dreamteam.service.external;

import org.hack19.dreamteam.db.model.RequestStatus;
import org.hack19.dreamteam.db.model.ServiceRequestEntity;

public interface ExternalServiceProcessor {
    ServiceRequestEntity processRequest(ServiceRequestEntity serviceRequestEntity);

    ServiceRequestEntity externalServiceCallback(Long serviceRequestId, RequestStatus status);
}
