package org.hack19.dreamteam.service.external;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.db.model.RequestStatus;
import org.hack19.dreamteam.db.model.ServiceRequestEntity;
import org.hack19.dreamteam.db.repository.ServiceRequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Service
public class MockExternalService implements ExternalServiceProcessor {

    private final ServiceRequestRepository serviceRequestRepository;

    @Override
    public ServiceRequestEntity processRequest(ServiceRequestEntity serviceRequestEntity) {
        RequestStatus status = serviceRequestEntity.getTargetService().getId() == 7L ? RequestStatus.PENDING : RequestStatus.IN_PROGRESS;
        ServiceRequestEntity saved = externalServiceCallback(serviceRequestEntity.getId(), status);// This line mocks external service interaction
        return saved;
    }

    @Override
    public ServiceRequestEntity externalServiceCallback(Long serviceRequestId, RequestStatus status) {
        Optional<ServiceRequestEntity> byId = serviceRequestRepository.findById(serviceRequestId);
        ServiceRequestEntity requestEntity = byId.orElseThrow(() -> new RuntimeException("Request Not Found"));

        requestEntity.setStatus(status);
        return serviceRequestRepository.save(requestEntity);
    }
}
