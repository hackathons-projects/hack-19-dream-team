package org.hack19.dreamteam.service;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.db.repository.TargetServiceRepository;
import org.hack19.dreamteam.model.dto.TargetServiceDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class TargetServiceService {

    private final TargetServiceRepository targetServiceRepository;

    public List<TargetServiceDto> getAllTargetServices() {
        return targetServiceRepository.findAll().stream().map(TargetServiceDto::of).collect(Collectors.toList());
    }
}
