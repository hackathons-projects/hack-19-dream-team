package org.hack19.dreamteam.service.external;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.db.model.TargetServiceEntity;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ExternalServiceFactory {

    private final MockExternalService mockExternalService;

    // TODO this is mock factory implementation
    public ExternalServiceProcessor getExternalService(TargetServiceEntity targetService) {
        return mockExternalService;
    }

}
