package org.hack19.dreamteam.service;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.db.model.RequestStatus;
import org.hack19.dreamteam.db.model.ServiceRequestEntity;
import org.hack19.dreamteam.db.model.TargetServiceEntity;
import org.hack19.dreamteam.db.model.UserEntity;
import org.hack19.dreamteam.db.repository.ServiceRequestRepository;
import org.hack19.dreamteam.db.repository.TargetServiceRepository;
import org.hack19.dreamteam.db.repository.UserRepository;
import org.hack19.dreamteam.model.dto.ServiceRequestDto;
import org.hack19.dreamteam.model.payload.CreateRequestRequest;
import org.hack19.dreamteam.service.external.ExternalServiceFactory;
import org.hack19.dreamteam.service.external.ExternalServiceProcessor;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class ServiceRequestService {

    private final ExternalServiceFactory externalServiceFactory;
    private final UserRepository userRepository;
    private final TargetServiceRepository targetServiceRepository;
    private final ServiceRequestRepository serviceRequestRepository;
    private final EntityManagerFactory entityManagerFactory;

    public List<ServiceRequestDto> getAllRequests() {
        return serviceRequestRepository.findAll().stream().map(ServiceRequestDto::of).collect(Collectors.toList());
    }

    public ServiceRequestDto createRequest(CreateRequestRequest serviceRequestDto, String email) {
        ServiceRequestEntity requestEntity = new ServiceRequestEntity();
        requestEntity.setStatus(RequestStatus.CREATED);
        requestEntity.setContent(serviceRequestDto.getContent());

        UserEntity userEntity = userRepository.findByEmail(email).orElseThrow(() -> new RuntimeException("Creator user not found"));
        requestEntity.setCreator(userEntity);

        TargetServiceEntity targetServiceEntity = targetServiceRepository.findById(serviceRequestDto.getTargetServiceId()).orElseThrow(() -> new RuntimeException("Target service not found"));
        requestEntity.setTargetService(targetServiceEntity);

        ServiceRequestEntity saved = serviceRequestRepository.save(requestEntity);

        return ServiceRequestDto.of(saved);
    }

    public ServiceRequestDto processRequest(ServiceRequestDto serviceRequestDto) {

        ServiceRequestEntity saved = serviceRequestRepository.findById(serviceRequestDto.getId()).orElseThrow(() -> new RuntimeException("Result for process not found"));

        ExternalServiceProcessor externalService = externalServiceFactory.getExternalService(saved.getTargetService());
        ServiceRequestEntity result = externalService.processRequest(saved);

        saved = serviceRequestRepository.save(result);

        return ServiceRequestDto.of(saved);
    }

    public List<ServiceRequestDto> history(Long id) {
        AuditReader auditReader = AuditReaderFactory.get(entityManagerFactory.createEntityManager());
        AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(ServiceRequestEntity.class, true, true);
        q.add(AuditEntity.id().eq(id));
        List<ServiceRequestEntity> audit = q.getResultList();

        ServiceRequestEntity original = serviceRequestRepository.findById(id).orElseThrow(() -> new RuntimeException("Original record not found"));
        audit.forEach(serviceRequestEntity -> serviceRequestEntity.setCreator(original.getCreator()));

        return audit.stream().map(ServiceRequestDto::of).collect(Collectors.toList());
    }

}
