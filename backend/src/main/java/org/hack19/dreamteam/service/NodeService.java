package org.hack19.dreamteam.service;

import lombok.RequiredArgsConstructor;
import org.hack19.dreamteam.db.model.NodeEntity;
import org.hack19.dreamteam.db.repository.NodeRepository;
import org.hack19.dreamteam.model.dto.NodeDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class NodeService {

    private final NodeRepository nodeRepository;

    public NodeDto getStructureTree() {
        NodeEntity rootNode = nodeRepository.findByParentNull();
        return createStructure(rootNode);
    }

    private NodeDto createStructure(NodeEntity nodeEntity) {
        NodeDto node = NodeDto.of(nodeEntity);

        List<NodeEntity> children = nodeRepository.findAllByParent(nodeEntity);
        children.forEach(child -> {
            NodeDto structure = createStructure(child);
            node.getChildren().add(structure);
        });

        return node;
    }

    public NodeDto createCustomNode(NodeDto nodeDto) {
        NodeEntity customNode = new NodeEntity();
        NodeEntity saved = saveOrUpdate(customNode, nodeDto);
        saved.setIsCustom(true);
        saved.setWeight(1L);
        return NodeDto.of(saved);
    }

    public NodeDto updateNode(NodeDto nodeDto) {
        NodeEntity customNode = nodeRepository.findById(nodeDto.getId()).orElseThrow(() -> new RuntimeException("Not Found"));
        NodeEntity saved = saveOrUpdate(customNode, nodeDto);
        return NodeDto.of(saved);
    }

    private NodeEntity saveOrUpdate(NodeEntity customNode, NodeDto nodeDto) {
        customNode.setQuestion(nodeDto.getQuestion());
        customNode.setBundleText(nodeDto.getBundleText());
        customNode.setAdditionalInfo(nodeDto.getAdditionalInfo());

        NodeEntity byId = nodeRepository.findById(nodeDto.getParentId()).get();
        customNode.setParent(byId);
        return nodeRepository.save(customNode);
    }

    public Long incrementWeight(Long nodeId) {
        NodeEntity customNode = nodeRepository.findById(nodeId).orElseThrow(() -> new RuntimeException("Not Found"));
        if (customNode.getIsCustom()) {
            customNode.setWeight(customNode.getWeight() + 1);
            NodeEntity saved = nodeRepository.save(customNode);
            return saved.getWeight();
        } else {
            throw new RuntimeException("Not allowed increment not custom node");
        }
    }
}
