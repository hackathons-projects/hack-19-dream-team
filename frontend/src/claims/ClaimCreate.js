import React, { Component } from 'react';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import {
	Container,
	Select,
	MenuItem,
	FormControl,
	InputLabel,
	Button,
	Paper,
	Typography,
	TextField
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { parse } from 'query-string';

import {
	GET_LIST,
	CREATE,
	withDataProvider,
	Create,
	Edit,
	SimpleForm,
	DisabledInput,
	TextInput,
	DateInput,
	LongTextInput,
	ReferenceManyField,
	Datagrid,
	DateField,
	EditButton,
	Loading
} from 'react-admin';

import {
	ShoppingCart,
	School,
	LocalHospital,
	Domain,
	LocalTaxi,
	BusinessCenter
} from '@material-ui/icons';

const styles = {
	resulting: {
		color: '#333',
		marginBottom: '24px'
	},
	value: {
		color: 'rgba(0, 0, 0, 0.54)',
		fontSize: '13px',
		marginBottom: '16px'
	},
	bundle: {
		marginBottom: '16px'
	},
	inputLabel: {
		fontSize: '13px',
		marginBottom: '5px'
	},
	questionSmall: {
		fontWeight: 'bold',
		marginBottom: '4px'
	},
	select: {
		width: '100%'
	},
	selectInput: {
		root: {
			width: '100%'
		},
		input: {
		  '&:after': {
			  borderBottom: `2px solid #0173c1`,
		  },
			'&:before': {
				borderBottom: `1px solid #333333`,
			}
		}
	},
	actions: {
		display: 'flex',
		justifyContent: 'flex-end'
	},
	question: {
		textTransform: 'capitalize',
		marginBottom: '35px'
	},
	btn: {
		background: '#0173c1',
		'&:hover': {
			background: '#2862AC'
		}
	},
	mainTitle: {
		marginBottom: '24px',
		color: '#333'
	},
	subTitle: {
		marginBottom: '16px',
		color: '#333'
	},
	paper: {
		color: '#0063b0',
		padding: '24px',
	},
	link: {
		textDecoration: 'none'
	},
	iconWrapper: {
		display: 'block',
		textAlign: 'center',
		marginBottom: '24px',
		marginTop: '8px'
	},
	formControl: {
		marginBottom: '24px'
	},
	icon: {
		fontSize: '60px',
	},
	title: {
		color: '#333',
		marginBottom: '24px'
	},
	textField: {
		width: '100%'
	}
};

class ClaimCreate extends Component {
	state = {
		nodes: null,
		resultingList: [],
		activeNode: null,
		activeSelect: '',
		additionalQuestions: null,
		isLoading: false
	};

	handleSelectChange = (event) => this.setState(
		({ activeNode, activeSelect, resultingList }) => {

			let additionalQuestions = null;
			const activeSelectNode = activeNode.children.find(({ id }) => id == event.target.value);

			console.log(activeSelect, activeSelectNode)
			try {
				if (activeSelectNode && activeSelectNode.additionalInfo) {
					console.log('good');
					additionalQuestions = JSON.parse(activeSelectNode.additionalInfo);
				}
			} catch (e) {
				// do nothing
			}

			if (event.target.value === 'other') {
				additionalQuestions = { other: { label: 'Описание проблемы', type: 'text', value: '' } };
			}

			const newResultingList = [...resultingList];

			if ( event.target.value == 'other' || !event.target.value || (newResultingList.length && newResultingList[newResultingList.length - 1].parentId == activeSelectNode.parentId)) {
				newResultingList.pop();
			}

			return {
				additionalQuestions,
				activeSelect: event.target.value,
				resultingList: [...newResultingList, { ...activeSelectNode }]
			};
		}
	)

	async submit() {
		const { resultignList, activeSelectNode } = this.state;
		const { history, dataProvider } = this.props;

		this.setState({ isLoading: true });

		const { data: request } = await dataProvider(CREATE, 'requests', {
			data: {
				targetServiceId: activeSelectNode && activeSelectNode || 7,
				content: JSON.stringify(resultignList)
			}
		});

		this.setState({ isLoading: false });

		history.push(`/requests/${request.id}`);
	}

	handleAdditionalInfoChange = (name, value) => this.setState(
		({ additionalQuestions }) => {
			return {
				additionalQuestions: {
					...additionalQuestions,
					[name]: {
						...additionalQuestions[name],
						value
					}
				}
			};
		}
	)

	handleNextStep = () => this.setState(({ activeNode, activeSelect }) => ({
		activeNode: activeNode.children.find(({ id }) => id == activeSelect),
		additionalQuestions: null
	}))

	componentDidMount() {
		this.fetchData();
	}

	componentDidUpdate(prevProps) {
		if (this.props.version !== prevProps.version) {
			this.fetchData();
		}
	}

	async fetchData() {
		const { history, dataProvider } = this.props;

		const { data: nodes } = await dataProvider(GET_LIST, 'nodes', {});

		const rootId = this.getRoot();

		if (!rootId || !nodes || !nodes.length || !nodes[0].children.find(({ id }) => id == rootId)) {
			history.push('/')
		}
		console.log(nodes);
		
		this.setState({
			nodes,
			activeNode: nodes[0].children.find(({ id }) => id == rootId),
			rootNode: nodes[0].children.find(({ id }) => id == rootId)
		});
	}

	getRoot() {
		const {
			location: {
				search
			}
		} = this.props;

		const { node } = parse(search, { arrayFormat: 'bracket' });

		return node;
	}

	getIcon(id, className) {
		switch (id) {
			case 2:
				return <ShoppingCart className={className} />;
			case 3:
				return <School className={className} />;
			case 4:
				return <LocalHospital className={className} />;
			case 5:
				return <Domain className={className} />;
			case 6:
				return <LocalTaxi className={className} />;
			case 7:
				return <BusinessCenter className={className} />;
			default:
				return <BusinessCenter className={className} />;
		}
	}

	render() {
		const { activeNode, resultingList, isLoading, rootNode, nodes, activeSelect, additionalQuestions } = this.state;
		const { classes } = this.props;

		return (
			<Container maxWidth="sm">
				{nodes && activeNode && !isLoading
					? (
						<Paper className={classes.paper}>
							<Typography
								className={classes.title}
								variant="h4"
								component="h2"
								align="center"
							>
								Создание обращения
							</Typography>
							<Typography
								className={classes.question}
								variant="h5"
								component="h3"
								align="center"
							>
								Тема: {rootNode.bundleText}
							</Typography>
							{!!resultingList && !!resultingList.length && (
								<div className={classes.resulting}>
									<div className={classes.value}>
										{rootNode.bundleText}
									</div>
									{resultingList.map(({ id, bundleText, question }) => (
										<div>
											{!!question && (
												<div className={classes.value}>
													<div className={classes.questionSmall}>
														{question}
													</div>
													<div className={classes.bundle}>
														{bundleText}
													</div>
												</div>
											)}
										</div>
									))}
								</div>
							)}
							{!!activeNode.children && !!activeNode.children.length
								? (
									<div>
										<div className={classes.formControl}>
											<InputLabel className={classes.inputLabel}>
												{!!activeNode.question && `${activeNode.question[0].toUpperCase()}${activeNode.question.slice(1)}` }
											</InputLabel>
											<Select
												className={classes.select}
												value={this.state.activeSelect}
												classes={classes.selectInput}
												onChange={this.handleSelectChange}
												placeholder="Выбрать..."
												displayEmpty
											>
												<MenuItem value="">
													Выбрать...
												</MenuItem>
												{activeNode.children.map(({ id, bundleText }) => (
													<MenuItem key={id} value={id}>
														{bundleText}
													</MenuItem>
												))}
												<MenuItem value="other">
													Другое
												</MenuItem>
											</Select>
										</div>
										{additionalQuestions
											&& Object.keys(additionalQuestions).map(key => {
												const question = additionalQuestions[key];
												console.log('quest', question, 'q')
												if (question.type === 'date') {
													return (
														<div key={key} className={classes.formControl}>
															<InputLabel>
																{question.label}
															</InputLabel>
															<TextField
																value={question.value}
																onChange={(event) => this.handleAdditionalInfoChange(key, event.target.value)}
																type="date"
																className={classes.textField}
																InputLabelProps={{
																	shrink: true,
																}}
															/>
														</div>
													);
												}

												return (
													<div key={key}  className={classes.formControl}>
														<InputLabel>
															{question.label}
														</InputLabel>
														<TextField
															type="text"
															key={key}
															onChange={(event) => this.handleAdditionalInfoChange(key, event.target.value)}
															className={classes.textField}
															InputLabelProps={{
																shrink: true,
															}}
														/>
													</div>
												)
											})}
											<div className={classes.actions}>
												<Button
													className={classes.btn}
													disabled={!this.state.activeSelect
														|| (this.state.additionalQuestions && Object.keys(this.state.additionalQuestions).find((key) => !this.state.additionalQuestions[key].value))}
													variant="contained"
													color="primary"
													onClick={this.handleNextStep}
												>
													Далее
												</Button>
											</div>
									</div>
								)
								: (
									<div>
										{!!resultingList && !!resultingList.length && (
											<div className={classes.resulting}>
												<div className={classes.value}>
													{rootNode.bundleText}
												</div>
												{resultingList.map(({ id, bundleText, question }) => (
													<div>
														{question && (
															<div className={classes.value}>
																<div className={classes.questionSmall}>
																	{question}
																</div>
																<div className={classes.bundle}>
																	{bundleText}
																</div>
															</div>
														)}
													</div>
												))}
											</div>
										)}
										<div className={classes.actions}>
											<Button
												className={classes.btn}
												variant="contained"
												color="primary"
												onClick={() => this.submit()}
											>
												Отправить
											</Button>
										</div>
									</div>
								)
							}
						</Paper>
					)
					: <Loading size={25} thickness={2} />}
			</Container>
		);
	}
}

const mapStateToProps = state => ({
	version: state.admin.ui.viewVersion,
});

export default compose(
	withStyles(styles),
	withRouter,
	connect(mapStateToProps),
	withDataProvider
)(ClaimCreate);
