import React from 'react';
import { List, DatagridBody, TextField, EmailField, DateField, ShowButton } from 'react-admin';
import { Container } from '@material-ui/core';

import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import Datagrid from '../layout/Datagrid';
import Pagination from '../layout/Pagination';

const MyDatagridRow = ({ record, resource, id, onToggleItem, children, selected, basePath }) => (
    <TableRow key={id}>
        {React.Children.map(children, field => (
            <TableCell key={`${id}-${field.props.source}`}>
                {React.cloneElement(field, {
                    record,
                    basePath,
                    resource,
                })}
            </TableCell>
        ))}
    </TableRow>
);

const MyTextField = ({ source, record, transform }) => transform ? transform(record[source], record) : <span>{record[source]}</span>;

const transformStatus = (status) => {
	switch (status.toUpperCase()) {
		case 'CREATED':
			return 'Отправлено';
		case 'PENDING':
			return 'На модерации';
		case 'IN_PROGRESS':
			return 'В работе';
		case 'IN_PROGRESS':
			return 'Выполнено';
	}
};

const MyDatagridBody = props => <DatagridBody {...props} row={<MyDatagridRow />} />;
const MyDatagrid = props => <Datagrid {...props} hasBulkActions body={<MyDatagridBody />} />;

const ClaimList = props => (
	<Container>
		<List actions={null}  selectable={false} {...props} pagination={<Pagination />}>
			<MyDatagrid hasBulkActions={false} rowClick="edit">
				<TextField label="id" source="id" />
				<EmailField label="Email" source="email" />
				<MyTextField label="Статус" source="status" transform={transformStatus} />
				<DateField label="Дата создания" source="creationDate" />
				<DateField label="Дата изменения" source="modifiedDate" />
				<ShowButton label="Подробнее" />
			</MyDatagrid>
		</List>
	</Container>
);

export default ClaimList;
