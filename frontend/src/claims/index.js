import InvoiceIcon from '@material-ui/icons/LibraryBooks';

import ClaimCreate from './ClaimCreate';
import ClaimShow from './ClaimShow';
import ClaimList from './ClaimList';

export default {
	create: ClaimCreate,
	list: ClaimList,
	show: ClaimShow,
	icon: InvoiceIcon,
};
