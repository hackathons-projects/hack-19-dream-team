import React from 'react';
import { Show, SimpleShowLayout, TextField, DateField, EditButton, RichTextField } from 'react-admin';
import {  Avatar, Icon, Container } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Timeline } from 'react-material-timeline';

const transformStatus = (status) => {
	switch (status.toUpperCase()) {
		case 'CREATED':
			return 'Отправлено';
		case 'PENDING':
			return 'На модерации';
		case 'IN_PROGRESS':
			return 'В работе';
		case 'IN_PROGRESS':
			return 'Выполнено';
	}
};

const styles = {
	line: {
		background: '#0063b0',
	}
};

const TimelineField = ({ classes, source, record, transform }) => {
	const events = record[source] 
		? record[source] .map(
			({ status, modifiedDate }) => ({
				title: transformStatus(status),
				subheader: new Date(modifiedDate).toDateString(),
				description: [ 'Произошло какое-то событие с обращением' ],
				icon: <Avatar><Icon>home</Icon></Avatar>
			})
		)
		: null;

	return events && <Timeline events={events} classes={classes} />
};

const EnhancedTimeline = withStyles(styles)(TimelineField)

export const ClaimShow = (props) => (
	<Container>
		<Show {...props}>
			<SimpleShowLayout>
				<TextField source="id" />
				<TimelineField source="history" />
			</SimpleShowLayout>
		</Show>
	</Container>
);

export default ClaimShow;
