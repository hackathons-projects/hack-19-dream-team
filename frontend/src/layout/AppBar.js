import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import SettingsIcon from '@material-ui/icons/List';
import { withStyles, createStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import withWidth from '@material-ui/core/withWidth';
import compose from 'recompose/compose';
import { Link, Route } from 'react-router-dom';
import { UserMenu, MenuItemLink, Headroom, toggleSidebar as toggleSidebarAction } from 'react-admin';

const styles = {
	appBar: {
		background: '#0173c1'
	},
	toolbar: {
		paddingRight: 24,
	},
	title: {
		flex: 1,
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		color: '#E91E63',
		textTransform: 'uppercase',
		fontWeight: 'bold',
		fontSize: '18px',
		textDecoration: 'none'
	},
	subTitle: {
		color: '#ffffff'
	},
	mainTitle: {
		padding: '50px 24px',
		color: '#fff',
		background: '#2862AC'
	},
};

const CustomUserMenu = ({ translate, ...props }) => (
	<UserMenu {...props}>
		<MenuItemLink
			to="/requests"
			primaryText="Мои обращения"
			leftIcon={<SettingsIcon />}
		/>
	</UserMenu>
);

const Banner = ({ classes }) => (
	<Typography
		className={classNames(classes.question, classes.mainTitle)}
		variant="h4"
		component="h1"
		align="center"
	>
		Единое окно коммуникации с любым органом государственной власти
	</Typography>
);

const BannerEnhanced = withStyles(styles)(Banner);

const AppBar = ({
	children,
	classes,
	className,
	logo,
	logout,
	open,
	title,
	toggleSidebar,
	userMenu,
	width,
	...rest
}) => (
	<div>
		<Headroom>
			<MuiAppBar
				className={classNames(className, classes.appBar)}
				color="primary"
				position="static"
				{...rest}
			>
				<Container>
					<Toolbar
						disableGutters
						variant={width === 'xs' ? 'regular' : 'dense'}
						className={classes.toolbar}
					>
						<Link
							to="/"
							className={classes.title}
						>
							<span className={classes.subTitle}>Взаимодействие</span> 2.0
						</Link>
						<CustomUserMenu logout={logout} />
					</Toolbar>
				</Container>
			</MuiAppBar>
		</Headroom>
		<Route exact path="/" component={BannerEnhanced} />,
	</div>
);

AppBar.propTypes = {
	children: PropTypes.node,
	classes: PropTypes.object,
	className: PropTypes.string,
	logout: PropTypes.element,
	open: PropTypes.bool,
	title: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
		.isRequired,
	toggleSidebar: PropTypes.func,
	userMenu: PropTypes.node,
	width: PropTypes.string,
};

AppBar.defaultProps = {
	userMenu: <UserMenu />,
};

const enhance = compose(
	withStyles(styles),
	withWidth()
);

export default enhance(AppBar);
