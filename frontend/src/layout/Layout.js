import React from 'react';
import { Layout } from 'react-admin';

import AppBar from './AppBar';
import theme from './themes';

const Hide = props => null;
const CustomLayout = props => <Layout {...props} theme={theme} appBar={AppBar} sidebar={Hide} menu={Hide} />;

export default CustomLayout;
