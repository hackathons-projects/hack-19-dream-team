import React, { Component } from 'react';
import { GET_LIST, withDataProvider, Loading } from 'react-admin';
import compose from 'recompose/compose';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
	ShoppingCart,
	School,
	LocalHospital,
	Domain,
	LocalTaxi,
	BusinessCenter
} from '@material-ui/icons';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
	Container,
	Grid,
	Paper,
	Typography
} from '@material-ui/core';

const styles = {
	question: {
		textTransform: 'capitalize'
	},
	subTitle: {
		marginBottom: '24px',
		marginTop: '-10px',
		color: '#333'
	},
	paper: {
		color: '#0063b0',
		height: '160px',
		padding: '24px'
	},
	link: {
		textDecoration: 'none'
	},
	iconWrapper: {
		display: 'block',
		textAlign: 'center',
		marginBottom: '24px',
		marginTop: '8px'
	},
	icon: {
		fontSize: '60px',
	}
};

class Dashboard extends Component {
	state = { nodes: null };

	componentDidMount() {
		this.fetchData();
	}

	componentDidUpdate(prevProps) {
		if (this.props.version !== prevProps.version) {
			this.fetchData();
		}
	}

	async fetchData() {
		const { dataProvider } = this.props;

		const { data: nodes } = await dataProvider(GET_LIST, 'nodes', {});

		this.setState({ nodes });
	}

	getIcon(id, className) {
		switch (id) {
			case 2:
				return <ShoppingCart className={className} />;
			case 3:
				return <School className={className} />;
			case 4:
				return <LocalHospital className={className} />;
			case 5:
				return <Domain className={className} />;
			case 6:
				return <LocalTaxi className={className} />;
			case 7:
				return <BusinessCenter className={className} />;
			default:
				return <BusinessCenter className={className} />;
		}
	}

	render() {
		const { nodes } = this.state;
		const { classes } = this.props;

		const root = nodes && nodes.length ? nodes[0] : null;

		return (
			<div>
				{root
					? (
						<div>
							<div className={classes.top}>
								<Container>
									<Typography
										className={classNames(classes.subTitle)}
										variant="h5"
										component="h2"
										align="center"
									>
										Составить обращение в сфере:
									</Typography>
								</Container>
							</div>
							<Container>
								<Grid container spacing={4}>
									{root.children.map(({ id, question, bundleText }) => (
										<Grid item xs={4} key={id}>
											<Link to={`/requests/create?node=${id}`} className={classes.link}>
												<Paper className={classes.paper}>
													<div className={classes.iconWrapper}>
														{this.getIcon(id, classes.icon)}
													</div>
													<Typography
														className={classes.question}
														variant="h5"
														component="h3"
														align="center"
													>
														{bundleText}
													</Typography>
												</Paper>
											</Link>
										</Grid>
									))}
								</Grid>
							</Container>
						</div>
					)
					: <Loading />}
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	version: state.admin.ui.viewVersion,
});

export default compose(
	withStyles(styles),
	connect(mapStateToProps),
	withDataProvider
)(Dashboard);
