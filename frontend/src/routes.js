import React from 'react';
import { Route } from 'react-router-dom';
import Configuration from './configuration/Configuration';
import Segments from './segments/Segments';
import OAuth2RedirectHandler from './user/oauth2/OAuth2RedirectHandler';

export default [
    <Route exact path="/configuration" component={Configuration} />,
    <Route exact path="/segments" component={Segments} />,
	<Route path="/oauth2/redirect" component={OAuth2RedirectHandler} />,
];
