import React from 'react';
import { Admin, Resource, fetchUtils, ShowGuesser } from 'react-admin';
import russianMessages from 'ra-language-russian';

import jsonServerProvider, { httpClient } from './dataProvider'
import authProvider from './authProvider';

import { Login, Layout } from './layout';
import { Dashboard } from './dashboard';

import { createMuiTheme } from '@material-ui/core/styles';
import claims from './claims';

const messages = {
	ru: russianMessages,
};

const myTheme = createMuiTheme({
	palette: {
		primary: {
			500: '#0173c1',
		}
	},
});

const dataProvider = jsonServerProvider('http://localhost:5000', httpClient);

const App = () => (
	<Admin
		title=""
		theme={myTheme}
		locale="ru"
		messages={messages}
		dataProvider={dataProvider}
		authProvider={authProvider}
		dashboard={Dashboard}
		loginPage={Login}
		appLayout={Layout}
	>
		<Resource name="requests" {...claims} />
		<Resource name="nodes" />
	</Admin>
);

export default App;
