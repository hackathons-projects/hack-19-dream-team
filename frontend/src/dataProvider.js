import { stringify } from 'query-string';
import {
	fetchUtils,
	GET_LIST,
	GET_ONE,
	GET_MANY,
	GET_MANY_REFERENCE,
	CREATE,
	UPDATE,
	UPDATE_MANY,
	DELETE,
	DELETE_MANY,
} from 'react-admin';

export default (apiUrl, httpClient = fetchUtils.fetchJson) => {
	const convertDataRequestToHTTP = (type, resource, params) => {
		let url = '';
		const options = {};
		switch (type) {
			case GET_LIST: {
				const page = params.pagination ? params.pagination.page : null;
				const perPage = params.pagination ? params.pagination.perPage : null;

				const field = params.pagination ? params.pagination.field : null;
				const order = params.sort ? params.sort.order : null;

				let query = {};

				if (page && perPage) {
					query._start = (page - 1) * perPage;
					query._end = page * perPage;
				}

				if (field && order) {
					query._sort = field;
					query._order = order;
				}

				if (params.filter) {
					query = {
						...query,
						...fetchUtils.flattenObject(params.filter)
					};
				}

				url = Object.keys(query).length
					? `${apiUrl}/${resource}?${stringify(query)}`
				   	: `${apiUrl}/${resource}`;
				break;
			}
			case GET_ONE:
				url = `${apiUrl}/${resource}/${params.id}`;
				break;
			case GET_MANY_REFERENCE: {
				const { page, perPage } = params.pagination;
				const { field, order } = params.sort;
				const query = {
					...fetchUtils.flattenObject(params.filter),
					[params.target]: params.id,
					_sort: field,
					_order: order,
					_start: (page - 1) * perPage,
					_end: page * perPage,
				};
				url = `${apiUrl}/${resource}?${stringify(query)}`;
				break;
			}
			case UPDATE:
				url = `${apiUrl}/${resource}/${params.id}`;
				options.method = 'PUT';
				options.body = JSON.stringify(params.data);
				break;
			case CREATE:
				url = `${apiUrl}/${resource}`;
				options.method = 'POST';
				options.body = JSON.stringify(params.data);
				break;
			case DELETE:
				url = `${apiUrl}/${resource}/${params.id}`;
				options.method = 'DELETE';
				break;
			case GET_MANY: {
				const query = {
					id: params.ids,
				};
				url = `${apiUrl}/${resource}?${stringify(query)}`;
				break;
			}
			default:
				throw new Error(`Unsupported fetch action type ${type}`);
		}
		return { url, options };
	};

	const convertHTTPResponse = (response, type, resource, params) => {
		const { headers, json } = response;
		switch (type) {
			case GET_LIST:
			case GET_MANY_REFERENCE:
				return {
					data: resource === 'nodes' ? [json] : json,
					total: headers.get('x-total-count')
						? parseInt(
							headers
								.get('x-total-count')
								.split('/')
								.pop(),
							10
						)
						: 10,
				};
			case CREATE:
				return { data: { ...params.data, id: json.id } };
			case GET_ONE:
				return { data: resource === 'requests' ? { id: params.id, history: json } : json };
			default:
				return { data: json };
		}
	};

	return (type, resource, params) => {
		// json-server doesn't handle filters on UPDATE route, so we fallback to calling UPDATE n times instead
		if (type === UPDATE_MANY) {
			return Promise.all(
				params.ids.map(id =>
					httpClient(`${apiUrl}/${resource}/${id}`, {
						method: 'PUT',
						body: JSON.stringify(params.data),
					})
				)
			).then(responses => ({
				data: responses.map(response => response.json),
			}));
		}

		if (type === DELETE_MANY) {
			return Promise.all(
				params.ids.map(id =>
					httpClient(`${apiUrl}/${resource}/${id}`, {
						method: 'DELETE',
					})
				)
			).then(responses => ({
				data: responses.map(response => response.json),
			}));
		}
		const { url, options } = convertDataRequestToHTTP(
			type,
			resource,
			params
		);
		return httpClient(url, options).then(response =>
			convertHTTPResponse(response, type, resource, params)
		);
	};
};

export const httpClient = (url, options = {}) => {
	if (!options.headers) {
		options.headers = new Headers({ Accept: 'application/json' });
	}

	options.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);

	return fetchUtils.fetchJson(url, options);
}
